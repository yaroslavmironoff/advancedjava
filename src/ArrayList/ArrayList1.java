package ArrayList;

import java.util.ArrayList;
import java.util.List;

public class ArrayList1 {
    public static void main(String[] args) {
        ArrayList arrayList1 = new ArrayList<>();
        arrayList1.add("Ivan");
        arrayList1.add("Petya");
        arrayList1.add("Kolya");
        arrayList1.add(7);
        arrayList1.add(new Car());
        System.out.println(arrayList1);

        ArrayList<String> arrayList2 = new ArrayList<>(200);
        arrayList2.add("Ann");
        arrayList2.add("Yura");
        List<String> arrayList3 = new ArrayList<>();
        ArrayList<String> arrayList4 = new ArrayList<>(arrayList1);
        System.out.println(arrayList4);
        System.out.println(arrayList1 == arrayList4);
    }
}

class Car {
};
